﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startLookingAroundScript : MonoBehaviour
{

    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void startAnimationLookingAround()
    {
        anim.SetTrigger("startLookingAround");    
    }
}
