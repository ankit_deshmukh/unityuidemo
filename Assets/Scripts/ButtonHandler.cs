﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour
{

    public GameObject Panel;

    public void setText(string str)
    {
        Text txt = transform.Find("Text").GetComponent<Text>();
        txt.text = str;

        if(Panel != null)
        {
            Panel.SetActive(true);
            Panel.transform.localScale = Vector3.Lerp(Panel.transform.localScale, Panel.transform.localScale * 2, Time.deltaTime * 10);
        }
    }

}
