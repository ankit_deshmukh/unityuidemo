﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;
using System.Runtime.InteropServices;

[System.Serializable]
public class MarkerPrefabs
{
    public string prefabName;
    public GameObject targetPrefab;
}

public class ImageRecognitionExample : MonoBehaviour
{

    public MarkerPrefabs[] markerPrefabCombos;

    private ARTrackedImageManager _arTrackedImageManager;

    private Camera m_MainCamera;
    
    private int count = 0;

    private bool isInTheFrame;

    private Vector3 prefabPosition;

    private GameObject objectInflatedOnScreen = null;

    public GameObject animatedGuy;

    private void Awake()
    {
        m_MainCamera = Camera.main;
        _arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

    private void OnEnable()
    {
        _arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }

    private void OnDestroy()
    {
        _arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }

    private void OnImageChanged(ARTrackedImagesChangedEventArgs args)
    {
        animatedGuy.SetActive(true);

        foreach (var trackedImage in args.updated)
        {
            Debug.Log("Added image name = " + trackedImage.referenceImage.name);
        }

        foreach (var trackedImage in args.updated)
        {
            Debug.Log("Updated Image Name = " + trackedImage.referenceImage.name);

            if (trackedImage.trackingState == TrackingState.Tracking || trackedImage.trackingState == TrackingState.Limited)
            {
                Debug.Log("Tracking State = " + trackedImage.trackingState.ToString());
                /* Loop through image/prefab-combo array */
                for (int i = 0; i < markerPrefabCombos.Length; i++)
                {
                    /* If trackedImage matches an image in the array */
                    if (markerPrefabCombos[i].prefabName == trackedImage.referenceImage.name)
                    {

                        /* Set the corresponding prefab to active at the center of the tracked image */
                        objectInflatedOnScreen = markerPrefabCombos[i].targetPrefab;
                        objectInflatedOnScreen.SetActive(true);
                        objectInflatedOnScreen.transform.position = trackedImage.transform.position;

                        //markerPrefabCombos[i].targetPrefab.SetActive(true);
                        //markerPrefabCombos[i].targetPrefab.transform.position = trackedImage.transform.position;

                        // Debug.Log("Screen to world port point = " + m_MainCamera.ScreenToWorldPoint(markerPrefabCombos[i].targetPrefab.transform.position));

                        prefabPosition = objectInflatedOnScreen.transform.position;

                        Vector3 animPos = prefabPosition;

                        animPos.x = (float)-0.07;

                        animatedGuy.transform.position = animPos;
                        
                        Debug.Log("Prefab Position = " + prefabPosition.ToString());

                        var rot = trackedImage.transform.rotation;
                        objectInflatedOnScreen.transform.rotation = rot * Quaternion.Euler(90,0,0);
                        animatedGuy.transform.rotation = rot * Quaternion.Euler(-90, 150, 0);

                    }
                }
                /* If not properly tracked 
                */
            }
            else
            {
                objectInflatedOnScreen.SetActive(false);

                Debug.Log("Tracking State = " + trackedImage.trackingState.ToString());
                /* Deactivate all prefabs */
                for (int i = 0; i < markerPrefabCombos.Length; i++)
                {
                    markerPrefabCombos[i].targetPrefab.SetActive(false);
                }
            }
        }

        foreach( var trackedImage in args.removed)
        {
            Debug.Log("Removed Image Name = " + trackedImage.referenceImage.name);
        }

        // If is in the frame
        //Debug.Log(trackedImage.name);
    }

    void Start()
    {
            
    }

    void Update()
    {
        count++;

        Vector3 newPoint = m_MainCamera.WorldToViewportPoint(prefabPosition);
        Debug.Log("New Point = " + newPoint.ToString());

        if (newPoint.z > 0 && newPoint.x > 0 && newPoint.x < 1 && newPoint.y > 0 && newPoint.y < 1)
            {
                isInTheFrame = true;
        }
        else
            {
                isInTheFrame = false;

            if(objectInflatedOnScreen!=null)
            {
                Debug.Log("Removing Object");
                objectInflatedOnScreen.SetActive(false);
                animatedGuy.SetActive(false);
            }

        }
    
    }


}
